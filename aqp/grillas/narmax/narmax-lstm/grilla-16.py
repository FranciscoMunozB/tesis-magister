#imports
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf
from tensorflow.keras.layers import Input, Dense, Flatten, LSTM
from tensorflow.keras.models import Model
from tensorflow.keras.optimizers import SGD, Adam
from keras.callbacks import EarlyStopping, ModelCheckpoint
from keras.models import load_model
import xlsxwriter
import openpyxl
import timeit


def cargarData1():
  trainDFScaled = pd.read_csv('data/dataTrainNarmaxScaledLSTM.csv', header=0)
  testDFScaled = pd.read_csv('data/dataTestNarmaxScaledLSTM.csv', header=0)
  trainDF = pd.read_csv('data/dataTrainNarmaxLSTM.csv', header=0)
  testDF = pd.read_csv('data/dataTestNarmaxLSTM.csv', header=0)
  return trainDFScaled, testDFScaled, trainDF, testDF



def createExcelNARX(name_file):
  # Cretae a xlsx file
  xlsx_File = xlsxwriter.Workbook(name_file)

  # Add new worksheet
  sheet_schedule = xlsx_File.add_worksheet()

  # write into the worksheet
  sheet_schedule.write('A1', 'MAE')
  sheet_schedule.write('B1', 'delayedSteps')
  sheet_schedule.write('C1', 'nameModel')
  sheet_schedule.write('D1', 'nameHistory')
  sheet_schedule.write('E1', 'nameConfiguration')
  sheet_schedule.write('F1', 'timeTrain (in seconds)')

  # Close the Excel file
  xlsx_File.close()


# calculate the error between an actual and predicted value
def calculate_error(actual, predicted):
	# calculate abs difference
	return abs(actual - predicted)


def evaluate_forecasts(predictions, testData):
  total_mae = 0.0
  total_c = 0
  for i in range(len(predictions)):
    real = testData[i]
    predicted = predictions[i]
    # calculate error
    error = calculate_error(real, predicted)
    # update statistics
    total_mae += error
    total_c += 1
  # normalize summed absolute errors
  total_mae /= total_c
  return total_mae


def generateSupervisedData(data, delays, predictionHorizon, columnTarget = 45):
  cont = 0
  X = list()
  y = list()

  #Se va a iterar para cada fragmento de datos
  values = data.values
  chunk_ids = np.unique(values[:, 0])
  for chunk_id in chunk_ids:
    rows = data.loc[(data['chunkID'] == chunk_id)]
    #print(rows)
    cantidadFilas = rows.shape[0]
    indexAux = 0
    indexTarget = 0
    for _, row in rows.iterrows():
      end_row = indexAux + delays
      indexTarget = end_row - 1 + predictionHorizon
      if indexTarget <= cantidadFilas-1:
        salidaAux = rows.iloc[indexTarget, columnTarget]
        xAux = rows.iloc[indexAux:end_row, :].values
        #print(xAux.shape)
        #print(xAux)
        X.append(xAux)
        y.append(salidaAux)
        #print(salidaAux)
      indexAux = indexAux + 1
    cont = cont + 1
  X = np.array(X)
  y = np.array(y)
  return X, y

def generateFinalData(delays, predictionHorizon, trainDFScaled, testDFScaled, trainDF, testDF):
  trainXScaled, trainYScaled = generateSupervisedData(trainDFScaled, delays, predictionHorizon)
  testXScaled, testYScaled = generateSupervisedData(testDFScaled, delays, predictionHorizon)
  trainX, trainY = generateSupervisedData(trainDF, delays, predictionHorizon)
  testX, testY = generateSupervisedData(testDF, delays, predictionHorizon)
  #print("trainXScaled.shape: ", str(trainXScaled.shape))
  #print("trainYScaled.shape: ", str(trainYScaled.shape))
  #print("testXScaled.shape: ", str(testXScaled.shape))
  #print("testYScaled.shape: ", str(testYScaled.shape))
  #print("trainX.shape: ", str(trainX.shape))
  #print("trainY.shape: ", str(trainY.shape))
  #print("testX.shape: ", str(testX.shape))
  #print("testY.shape: ", str(testY.shape))
  return trainXScaled, trainY, testXScaled, testY


def findMinMae(listMAE):
  minMae = 1
  for e in listMAE:
    if minMae > e:
      minMae = e
  return minMae


def escribirGrillaExcel(data, nameFile):
  my_file = nameFile
  my_wb_obj = openpyxl.load_workbook(my_file)
  my_sheet_obj = my_wb_obj.active

  row = my_sheet_obj.max_row + 1
  column = 1

  # Iterating through data list
  for elem in data:
    my_sheet_obj.cell(row=row, column=column).value = elem
    column += 1

  my_wb_obj.save(nameFile)


#funcion que crea un diccionario con la configuracion ingresada como paramentros de entrada
def generateConfigurationDictionary(nInputs, nOutputs, nLstmUnits, activationType, recurrentActivationType, typeOut, Loss, Delays, PredictionHorizon):
  configDict = {
      "nInputs": nInputs, #Corresponde a una tupla
      "nOutputs": nOutputs, #Corresponde a un escalar
      "nLstmUnits": nLstmUnits, #Corresponde a un entero
      "activationType": activationType, #Corresponde a un string
      "recurrentActivationType": recurrentActivationType, #Corresponde a un string
      "typeOut": typeOut, #corresponde al tipo de neurona de salida
      "Loss": Loss, #Corresponde a un string 
      "Delays": Delays, 
      "PredictionHorizon": PredictionHorizon
      }
  return configDict

def generateMultipleModelLSTM(trainXScaled, trainY, testXScaled, testY, delay, horizon, nameExcelFile):
  #para crear una configuracion, las siguientes variables se pasan como escalares
  nInputs = trainXScaled[0].shape
  nOutputs = 1
  nLstmUnits = [32, 64, 128, 256]
  activationType = ['sigmoid','tanh']
  recurrentActivationType = ['sigmoid','tanh']
  typeOut = ['relu', 'sigmoid', 'tanh']
  lossFunction = ['mean_squared_error', 'mean_absolute_error']

  for NLU in nLstmUnits:
    for AT in activationType:
      for RAT in recurrentActivationType:
        for TO in typeOut:
          for LF in lossFunction:
            configDict = generateConfigurationDictionary(nInputs, nOutputs, NLU, AT, RAT, TO, LF, delay, horizon)
            aplicateLSTM(configDict, trainXScaled, trainY, testXScaled, testY, nameExcelFile)



def aplicateLSTM(configDict, trainXScaled, trainY, testXScaled, testY, nameExcelFile):
  #generate name best model
  nNeuronInputLayersAux = ""
  for e in configDict['nInputs']:
    nNeuronInputLayersAux = nNeuronInputLayersAux + str(e)

  
  nameModel1 = "results/LSTM_"+nNeuronInputLayersAux+ "_"+str(configDict['nOutputs'])+"_"+str(configDict['nLstmUnits'])+ "_" + configDict['activationType'] +"_"+configDict['recurrentActivationType']
  nameModel1 = nameModel1 + "_"+configDict['typeOut']+"_"+ configDict['Loss'] +"_"+str(configDict['Delays'])+ "S"
  nameModel = nameModel1 + ".h5"

  #print(nameModel1)

  es = EarlyStopping(monitor='val_mean_absolute_error', mode='min', verbose=0, patience=60)
  mc = ModelCheckpoint(nameModel, monitor='val_mean_absolute_error', mode='min', verbose=0, save_best_only=True)


  # Build the model using the functional API
  inp = Input(shape=configDict['nInputs'])
  x = LSTM(units=configDict['nLstmUnits'], activation=configDict['activationType'],recurrent_activation=configDict['recurrentActivationType'])(inp)
  out = Dense(units=configDict['nOutputs'],activation=configDict['typeOut'])(x)
  model = Model(inp, out)

  model.compile(
    loss=configDict['Loss'],
    optimizer='adam', 
    metrics=[tf.keras.metrics.MeanSquaredError(), 
            tf.keras.metrics.RootMeanSquaredError(), 
            tf.keras.metrics.MeanAbsoluteError(), 
            tf.keras.metrics.MeanAbsolutePercentageError()
            ]
  )
  
  #train the model
  startTrainTime = timeit.default_timer()
  r = model.fit(trainXScaled, trainY, validation_data=(testXScaled, testY), epochs=120, verbose=0, callbacks=[es, mc])

  #Your statements here

  stopTrainTime = timeit.default_timer()
  totalTime = stopTrainTime - startTrainTime
  totalTime = float("{:.2f}".format(totalTime))


  # para recuperar el modelo
  #saved_model = load_model(nameModel)
  #print(saved_model.summary())
  #print(saved_model.layers[2].get_config())
  

  minMae = findMinMae(r.history['val_mean_absolute_error'])

  #save history
  nameHistory = nameModel1 + "_history.npy"
  np.save(nameHistory,r.history)

  #save config
  nameConfig = nameModel1 + "_config.npy"
  np.save(nameConfig,configDict)

  #para recuperar history
  #history=np.load(nameHistory,allow_pickle='TRUE').item()
  #print(history)

  #para recuperar configDict
  #config=np.load(nameConfig,allow_pickle='TRUE').item()
  #print(config)

  #escribir archivo csv con configuracion, mae y nombres de archivos (mejor modelo e historia)
  dataToWrite = [minMae, configDict['Delays'], nameModel, nameHistory, nameConfig, totalTime]
  escribirGrillaExcel(dataToWrite, nameExcelFile)



def runMultipleLSTM(trainDFScaled, testDFScaled, trainDF, testDF, nameFile, stepsAR):
  #Se procede a generar los ciclos que permiten ejecutar multiples pruebas
  #stepsAR = [2,4,8,16,24,32]
  #stepsAR = [24]
  predictionHorizon = 24
  for stepAR in stepsAR:
    #Generar data estructurada
    trainXScaled, trainY, testXScaled, testY = generateFinalData(stepAR, predictionHorizon, trainDFScaled, testDFScaled, trainDF, testDF)

    #probar multiples modelos MLP
    generateMultipleModelLSTM(trainXScaled, trainY, testXScaled, testY, stepAR, predictionHorizon, nameFile)




trainDFScaled, testDFScaled, trainDF, testDF = cargarData1()
stepsAR = [16]
nameFileLSTM = "results/grilla_LSTM_NARMAX_"+str(stepsAR[0])+"STEPS.xlsx"
createExcelNARX(nameFileLSTM)
runMultipleLSTM(trainDFScaled, testDFScaled, trainDF, testDF, nameFileLSTM, stepsAR)

