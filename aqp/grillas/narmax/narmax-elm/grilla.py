#imports
import pandas as pd
import numpy as np
from joblib import dump, load
import xlsxwriter
import openpyxl
import timeit
import time


def _mean_squared_error(y, pred):
    return 0.5 * np.mean((y - pred) ** 2)


def _mean_abs_error(y, pred):
    return np.mean(np.abs(y - pred))


def _sigmoid(x):
    return 1. / (1. + np.exp(-x))


def _identity(x):
    return x



def getActivation(name):
    return {
        'sigmoid': _sigmoid
    }[name]


def getLoss(name):
    return {
        'mse': _mean_squared_error,
        'mae': _mean_abs_error
    }[name]


class ELM:
    def __init__(self, num_input_nodes, num_hidden_units, num_out_units, activation='sigmoid',
                 loss='mse', beta_init=None, w_init=None, bias_init=None):
        self._num_input_nodes = num_input_nodes
        self._num_hidden_units = num_hidden_units
        self._num_out_units = num_out_units

        self._activation = getActivation(activation)
        self._loss = getLoss(loss)

        if isinstance(beta_init, np.ndarray):
            self._beta = beta_init
        else:
            self._beta = np.random.uniform(-1., 1., size=(self._num_hidden_units, self._num_out_units))

        if isinstance(w_init, np.ndarray):
            self._w = w_init
        else:
            self._w = np.random.uniform(-1, 1, size=(self._num_input_nodes, self._num_hidden_units))

        if isinstance(bias_init, np.ndarray):
            self._bias = bias_init
        else:
            self._bias = np.zeros(shape=(self._num_hidden_units,))

        #print('Bias shape:', self._bias.shape)
        #print('W shape:', self._w.shape)
        #print('Beta shape:', self._beta.shape)

    def fit(self, X, Y, display_time=False):
        H = self._activation(X.dot(self._w) + self._bias)

        # Moore–Penrose pseudo inverse
        if display_time:
            start = time.time()
        H_pinv = np.linalg.pinv(H)
        if display_time:
            stop = time.time()
            print(f'Train time: {stop-start}')

        self._beta = H_pinv.dot(Y)

        # print('Fit Beta shape:', self._beta.shape)

    def __call__(self, X):
        H = self._activation(X.dot(self._w) + self._bias)
        return H.dot(self._beta)

    def evaluate(self, X, Y):
        pred = self(X)

        # Loss (base on model setting)
        loss = self._loss(Y, pred)

        # Accuracy
        #acc = np.sum(np.argmax(pred, axis=-1) == np.argmax(Y, axis=-1)) / len(Y)

        # Unweighted Average Recall
        # TODO

        #return loss, acc, pred
        return loss, pred




def cargarData1():
  trainDFScaled = pd.read_csv('data/dataTrainNarmaxScaledELM.csv', header=0)
  testDFScaled = pd.read_csv('data/dataTestNarmaxScaledELM.csv', header=0)
  trainDF = pd.read_csv('data/dataTrainNarmaxELM.csv', header=0)
  testDF = pd.read_csv('data/dataTestNarmaxELM.csv', header=0)
  return trainDFScaled, testDFScaled, trainDF, testDF



def createExcelNARX(name_file):
  # Cretae a xlsx file
  xlsx_File = xlsxwriter.Workbook(name_file)

  # Add new worksheet
  sheet_schedule = xlsx_File.add_worksheet()

  # write into the worksheet
  sheet_schedule.write('A1', 'MAE')
  sheet_schedule.write('B1', 'delayedSteps')
  sheet_schedule.write('C1', 'nameModel')
  sheet_schedule.write('D1', 'nameConfiguration')
  sheet_schedule.write('E1', 'timeTrain (in seconds)')

  # Close the Excel file
  xlsx_File.close()


# calculate the error between an actual and predicted value
def calculate_error(actual, predicted):
	# calculate abs difference
	return abs(actual - predicted)


def evaluate_forecasts(predictions, testData):
  total_mae = 0.0
  total_c = 0
  for i in range(len(predictions)):
    real = testData[i]
    predicted = predictions[i]
    # calculate error
    error = calculate_error(real, predicted)
    # update statistics
    total_mae += error
    total_c += 1
  # normalize summed absolute errors
  total_mae /= total_c
  return total_mae


def generateSupervisedData(data, delays, predictionHorizon, columnTarget = 45):
  cont = 0
  X = list()
  y = list()

  #Se va a iterar para cada fragmento de datos
  values = data.values
  chunk_ids = np.unique(values[:, 0])
  for chunk_id in chunk_ids:
    rows = data.loc[(data['chunkID'] == chunk_id)]
    #print(rows)
    cantidadFilas = rows.shape[0]
    indexAux = 0
    indexTarget = 0
    for _, row in rows.iterrows():
      end_row = indexAux + delays
      indexTarget = end_row - 1 + predictionHorizon
      if indexTarget <= cantidadFilas-1:
        salidaAux = rows.iloc[indexTarget, columnTarget]
        xAux = rows.iloc[indexAux:end_row, :].values
        #print(xAux.shape)
        #print(xAux)
        X.append(xAux)
        y.append(salidaAux)
        #print(salidaAux)
      indexAux = indexAux + 1
    cont = cont + 1
  X = np.array(X)
  y = np.array(y)
  return X, y

def reshapeXdata(trainXScaled, testXScaled):
  #reshape data
  trainXScaled = trainXScaled.reshape((trainXScaled.shape[0],trainXScaled.shape[1]*trainXScaled.shape[2]))
  #print("trainXScaled.shape: ", str(trainXScaled.shape))
  testXScaled = testXScaled.reshape((testXScaled.shape[0],testXScaled.shape[1]*testXScaled.shape[2]))
  #print("testXScaled.shape: ", str(testXScaled.shape))
  return trainXScaled, testXScaled


def generateFinalData(delays, predictionHorizon, trainDFScaled, testDFScaled, trainDF, testDF):
  trainXScaled, trainYScaled = generateSupervisedData(trainDFScaled, delays, predictionHorizon)
  testXScaled, testYScaled = generateSupervisedData(testDFScaled, delays, predictionHorizon)
  trainX, trainY = generateSupervisedData(trainDF, delays, predictionHorizon)
  testX, testY = generateSupervisedData(testDF, delays, predictionHorizon)
  #print("trainXScaled.shape: ", str(trainXScaled.shape))
  #print("trainYScaled.shape: ", str(trainYScaled.shape))
  #print("testXScaled.shape: ", str(testXScaled.shape))
  #print("testYScaled.shape: ", str(testYScaled.shape))
  #print("trainX.shape: ", str(trainX.shape))
  #print("trainY.shape: ", str(trainY.shape))
  #print("testX.shape: ", str(testX.shape))
  #print("testY.shape: ", str(testY.shape))
  trainXScaled, testXScaled = reshapeXdata(trainXScaled, testXScaled)
  return trainXScaled, trainY, testXScaled, testY


def escribirGrillaExcel(data, nameFile):
  my_file = nameFile
  my_wb_obj = openpyxl.load_workbook(my_file)
  my_sheet_obj = my_wb_obj.active

  row = my_sheet_obj.max_row + 1
  column = 1

  # Iterating through data list
  for elem in data:
    my_sheet_obj.cell(row=row, column=column).value = elem
    column += 1

  my_wb_obj.save(nameFile)


#funcion que crea un diccionario con la configuracion ingresada como paramentros de entrada
def generateConfigurationDictionary(nInputs, nOutputs, nHiddenUnits, Loss, Delays, PredictionHorizon):
  configDict = {
      "nInputs": nInputs, #Corresponde a un escalar
      "nOutputs": nOutputs, #Corresponde a un escalar
      "nHiddenUnits": nHiddenUnits, #Corresponde a un escalar
      "Loss": Loss, #Corresponde a un string
      "Delays": Delays, 
      "PredictionHorizon": PredictionHorizon
      }
  return configDict


def generateMultipleModelELM(trainXScaled, trainY, testXScaled, testY, delay, horizon, nameExcelFile):
  #para crear una configuracion, las siguientes variables se pasan como escalares
  nInputs = trainXScaled.shape[1]
  nOutputs = 1
  nHiddenUnits = [10, 50, 100, 500, 1000, 2000, 4000]
  Loss = ['mse','mae']

  for NHU in nHiddenUnits:
    for L in Loss:
      configDict = generateConfigurationDictionary(nInputs, nOutputs, NHU, L, delay, horizon)
      aplicateELM(configDict, trainXScaled, trainY, testXScaled, testY, nameExcelFile)


def aplicateELM(configDict, trainXScaled, trainY, testXScaled, testY, nameExcelFile):
  #generate name best model
  
  nameModel1 = "results/ELM_"+str(configDict['nInputs'])+ "_"+str(configDict['nOutputs'])+"_"+str(configDict['nHiddenUnits'])
  nameModel1 = nameModel1 + "_"+configDict['Loss']+"_"+str(configDict['Delays'])+ "S"
  nameModel = nameModel1 + ".joblib"

  #print(nameModel1)


  regressor = ELM(num_input_nodes = configDict['nInputs'], num_hidden_units = configDict['nHiddenUnits'], num_out_units = configDict['nOutputs'], loss = configDict['Loss'])
  

  
  #train the model
  startTrainTime = timeit.default_timer()

  regressor.fit(trainXScaled, trainY)


  #Your statements here

  stopTrainTime = timeit.default_timer()
  totalTime = stopTrainTime - startTrainTime
  totalTime = float("{:.2f}".format(totalTime))

  val_loss, y_pred_LR = regressor.evaluate(testXScaled, testY)
    
  mae = evaluate_forecasts(y_pred_LR, testY)
  #print("MAE: " + str(mae))


  #Guardar modelo
  dump(regressor, nameModel) 

  #Para cargar modelo:
  #clf = load('filename.joblib') 

  #save config
  nameConfig = nameModel1 + "_config.npy"
  np.save(nameConfig,configDict)


  #para recuperar configDict
  #config=np.load(nameConfig,allow_pickle='TRUE').item()
  #print(config)

  #escribir archivo csv con configuracion, mae y nombres de archivos (mejor modelo e historia)
  dataToWrite = [mae, configDict['Delays'], nameModel, nameConfig, totalTime]
  escribirGrillaExcel(dataToWrite, nameExcelFile)


def runMultipleELM(trainDFScaled, testDFScaled, trainDF, testDF, stepAR, nameFile):
  predictionHorizon = 24
  trainXScaled, trainY, testXScaled, testY = generateFinalData(stepAR, predictionHorizon, trainDFScaled, testDFScaled, trainDF, testDF)
  #probar multiples modelos
  generateMultipleModelELM(trainXScaled, trainY, testXScaled, testY, stepAR, predictionHorizon, nameFile)


trainDFScaled, testDFScaled, trainDF, testDF = cargarData1()
stepsAR = [2, 4, 8, 16]
for stepAR in stepsAR:
  nameFileELM = "results/grilla_ELM_NARMAX_"+str(stepAR)+"STEPS.xlsx"
  createExcelNARX(nameFileELM)
  runMultipleELM(trainDFScaled, testDFScaled, trainDF, testDF, stepAR, nameFileELM)