#imports
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf
from tensorflow.keras.layers import Input, Dense
from tensorflow.keras.models import Model
from tensorflow.keras.optimizers import SGD, Adam
from keras.callbacks import EarlyStopping, ModelCheckpoint
from keras.models import load_model
import xlsxwriter
import openpyxl
import timeit

def cargarData1():
  trainDFScaled = pd.read_csv('data/dataTrainNarmaxScaledMLP.csv', header=0)
  testDFScaled = pd.read_csv('data/dataTestNarmaxScaledMLP.csv', header=0)
  trainDF = pd.read_csv('data/dataTrainNarmaxMLP.csv', header=0)
  testDF = pd.read_csv('data/dataTestNarmaxMLP.csv', header=0)
  return trainDFScaled, testDFScaled, trainDF, testDF



def createExcel(name_file):
  # Cretae a xlsx file
  xlsx_File = xlsxwriter.Workbook(name_file)

  # Add new worksheet
  sheet_schedule = xlsx_File.add_worksheet()

  # write into the worksheet
  sheet_schedule.write('A1', 'MAPE')
  sheet_schedule.write('B1', 'delayedSteps')
  sheet_schedule.write('C1', 'nameModel')
  sheet_schedule.write('D1', 'nameHistory')
  sheet_schedule.write('E1', 'nameConfiguration')
  sheet_schedule.write('F1', 'timeTrain (in seconds)')

  # Close the Excel file
  xlsx_File.close()


# calculate the error between an actual and predicted value
def mae(actual, predicted):
	# calculate abs difference
	return abs(actual - predicted)
 
def mape(actual,pred):
  return np.mean(np.abs((actual - pred) / actual)) * 100

def evaluate_forecasts(predictions, testData):
  total_mae = 0.0
  total_mape = 0.0
  total_c = 0
  for i in range(len(predictions)):
    real = testData[i]
    predicted = predictions[i]
    # calculate error
    error_mae = mae(real, predicted)
    error_mape = mape(real, predicted)
    # update statistics
    total_mae += error_mae
    total_mape += error_mape
    total_c += 1
  # normalize summed absolute errors
  total_mae /= total_c
  total_mape /= total_c
  return total_mae, total_mape


def generateSupervisedData(data, delays, predictionHorizon, columnTarget = 12):
  X = list()
  y = list()
  cantidadFilas = data.shape[0]
  indexAux = 0
  indexTarget = 0
  for _, row in data.iterrows():
    end_row = indexAux + delays
    indexTarget = end_row - 1 + predictionHorizon
    if indexTarget <= cantidadFilas-1:
      salidaAux = data.iloc[indexTarget, columnTarget]
      xAux = data.iloc[indexAux:end_row, :].values
      #print(xAux.shape)
      #print(xAux)
      X.append(xAux)
      y.append(salidaAux)
      #print(salidaAux)
    indexAux = indexAux + 1
  X = np.array(X)
  y = np.array(y)
  return X, y

def reshapeXdata(trainXScaled, testXScaled):
  #reshape data
  trainXScaled = trainXScaled.reshape((trainXScaled.shape[0],trainXScaled.shape[1]*trainXScaled.shape[2]))
  #print("trainXScaled.shape: ", str(trainXScaled.shape))
  testXScaled = testXScaled.reshape((testXScaled.shape[0],testXScaled.shape[1]*testXScaled.shape[2]))
  #print("testXScaled.shape: ", str(testXScaled.shape))
  return trainXScaled, testXScaled

def generateFinalData(delays, predictionHorizon, trainDFScaled, testDFScaled, trainDF, testDF):
  trainXScaled, trainYScaled = generateSupervisedData(trainDFScaled, delays, predictionHorizon)
  testXScaled, testYScaled = generateSupervisedData(testDFScaled, delays, predictionHorizon)
  trainX, trainY = generateSupervisedData(trainDF, delays, predictionHorizon)
  testX, testY = generateSupervisedData(testDF, delays, predictionHorizon)
  trainXScaled, testXScaled = reshapeXdata(trainXScaled, testXScaled)
  #print("trainXScaled.shape: ", str(trainXScaled.shape))
  #print("trainYScaled.shape: ", str(trainYScaled.shape))
  #print("testXScaled.shape: ", str(testXScaled.shape))
  #print("testYScaled.shape: ", str(testYScaled.shape))
  #print("trainX.shape: ", str(trainX.shape))
  #print("trainY.shape: ", str(trainY.shape))
  #print("testX.shape: ", str(testX.shape))
  #print("testY.shape: ", str(testY.shape))
  return trainXScaled, trainY, testXScaled, testY


#funcion que crea un diccionario con la configuracion ingresada como paramentros de entrada
def generateConfigurationDictionary(nInputs, nOutputs, nHiddenLayer, nNeuronHiddenLayers, typeNeuronHiddenLayers, typeOut, Loss, Delays, PredictionHorizon):
  configDict = {
      "nInputs": nInputs, #Corresponde a un escalar en el caso del MLP
      "nOutputs": nOutputs, #Corresponde a un escalar en el caso del MLP
      "nHiddenLayer": nHiddenLayer, #Corresponde a un escalar en el caso del MLP
      "nNeuronHiddenLayers": nNeuronHiddenLayers, #Corresponde a un vector de enteros del largo de 'nHiddenLayer'
      "typeNeuronHiddenLayers": typeNeuronHiddenLayers, #Corresponde a un vector de strings del largo de 'nHiddenLayer'
      "typeOut": typeOut, #corresponde al tipo de neurona de salida
      "Loss": Loss, #Corresponde a un string 
      "Delays": Delays, 
      "PredictionHorizon": PredictionHorizon
      }
  return configDict


def escribirGrillaExcel(data, nameFile):
  my_file = nameFile
  my_wb_obj = openpyxl.load_workbook(my_file)
  my_sheet_obj = my_wb_obj.active

  row = my_sheet_obj.max_row + 1
  column = 1

  # Iterating through data list
  for elem in data:
    my_sheet_obj.cell(row=row, column=column).value = elem
    column += 1

  my_wb_obj.save(nameFile)


def getNNHL1(trainXScaled):
  posibilidades = [[int(trainXScaled.shape[1]/2)], [40], [10], [5]]
  return posibilidades

def getNNHL2(trainXScaled):
  posibilidades = [[int(trainXScaled.shape[1]/2), 40], 
                   [int(trainXScaled.shape[1]/2), 10],
                   [int(trainXScaled.shape[1]/2), 5],
                   [40, 5],
                   [10, 5]]
  return posibilidades


def getNNHL3(trainXScaled):
  posibilidades = [[int(trainXScaled.shape[1]/2), 40, 10], 
                   [int(trainXScaled.shape[1]/2), 40, 5],
                   [int(trainXScaled.shape[1]/2), 10, 5],
                   [40, 10, 5]]
  return posibilidades


def getTNHL1():
  posibilidades = [['relu'], ['sigmoid'],['tanh']]
  return posibilidades


def getTNHL2():
  posibilidades = [['relu', 'relu'],
                   ['sigmoid', 'sigmoid'],
                   ['sigmoid', 'tanh'], 
                   ['tanh', 'sigmoid'],
                   ['tanh', 'tanh']]
  return posibilidades

def getTNHL3():
  posibilidades = [['relu', 'relu', 'relu'],
                   ['sigmoid', 'sigmoid', 'sigmoid'],
                   ['tanh', 'tanh', 'tanh'],
                   ['sigmoid', 'sigmoid', 'tanh'], 
                   ['tanh', 'sigmoid', 'tanh']]
  return posibilidades


def aplicateMLP(configDict, trainXScaled, trainY, testXScaled, testY, nameExcelFile):
  #generate name best model
  nNeuronHiddenLayersAux = ""
  for e in configDict['nNeuronHiddenLayers']:
    nNeuronHiddenLayersAux = nNeuronHiddenLayersAux + str(e)
  
  typeNeuronHiddenLayersAux = ""
  for e in configDict['typeNeuronHiddenLayers']:
    typeNeuronHiddenLayersAux = typeNeuronHiddenLayersAux + str(e)
  
  nameModel1 = "results/MLP_"+str(configDict['nInputs'])+ "_"+str(configDict['nOutputs'])+"_"+str(configDict['nHiddenLayer'])+ "_" + nNeuronHiddenLayersAux + "_" + typeNeuronHiddenLayersAux + "_"+ configDict['Loss'] + "_"+ str(configDict['typeOut'])
  nameModel1 = nameModel1 + "_"+str(configDict['Delays'])
  nameModel = nameModel1 + ".h5"
  
  es = EarlyStopping(monitor='val_mean_absolute_percentage_error', mode='min', verbose=1, patience=2000, restore_best_weights=True)
  #mc = ModelCheckpoint(nameModel, monitor='val_mean_absolute_error', mode='min', verbose=0, save_best_only=True)

  inp = Input(shape=(configDict['nInputs'],))

  x = Dense(configDict['nNeuronHiddenLayers'][0], activation=configDict['typeNeuronHiddenLayers'][0])(inp)

  for i in range(configDict['nHiddenLayer']-1):
    x = Dense(configDict['nNeuronHiddenLayers'][i+1], activation=configDict['typeNeuronHiddenLayers'][i+1])(x)
  
  out = Dense(configDict['nOutputs'],activation=configDict['typeOut'])(x)
  model = Model(inp, out)


  model.compile(
    loss=configDict['Loss'],
    optimizer='adam', 
    metrics=[tf.keras.metrics.MeanSquaredError(), 
            tf.keras.metrics.RootMeanSquaredError(), 
            tf.keras.metrics.MeanAbsoluteError(), 
            tf.keras.metrics.MeanAbsolutePercentageError()
            ]
  )
  #train the model
  startTrainTime = timeit.default_timer()
  r = model.fit(trainXScaled, trainY, validation_data=(testXScaled, testY), epochs=2000, verbose=0, callbacks=[es])

  #Your statements here

  stopTrainTime = timeit.default_timer()
  totalTime = stopTrainTime - startTrainTime
  totalTime = float("{:.2f}".format(totalTime))
  

  # Make predictions using the testing set
  y_pred = model.predict(testXScaled)
  mae, mape = evaluate_forecasts(y_pred, testY)

  # Guardar el Modelo
  model.save(nameModel)

  # para recuperar el modelo
  #saved_model = load_model(nameModel)
  #print(saved_model.summary())
  #print(saved_model.layers[2].get_config())

  #save history
  nameHistory = nameModel1 + "_history.npy"
  np.save(nameHistory,r.history)

  #save config
  nameConfig = nameModel1 + "_config.npy"
  np.save(nameConfig,configDict)

  #para recuperar history
  #history=np.load(nameHistory,allow_pickle='TRUE').item()
  #print(history)

  #para recuperar configDict
  #config=np.load(nameConfig,allow_pickle='TRUE').item()
  #print(config)

  #escribir archivo csv con configuracion, mae y nombres de archivos (mejor modelo e historia)
  dataToWrite = [mape, configDict['Delays'], nameModel, nameHistory, nameConfig, totalTime]
  escribirGrillaExcel(dataToWrite, nameExcelFile)


def generateMultipleModelMLP(trainXScaled, trainY, testXScaled, testY, delay, horizon, nameExcelFile):

  #para crear una configuracion, las siguientes variables se pasan como escalares
  nInputs = trainXScaled.shape[1]
  nOutputs = 1
  nHiddenLayer = [1,2,3]
  lossFunction = ['mean_squared_error','mean_squared_logarithmic_error', 'mean_absolute_error']

  # a continuacion se definen las cantidades de neuronas y los tipos de las capas ocultas
  #estas variables deben ser pasadas como vectores que dependen de la cantidad de capas ocultas
  #Sin embargo, se definen de manera estatica a continuacion

  #vector que contine la posible cantidad de neuronas de las capas ocultas
  #nNeuronHiddenLayers = [int(trainXScaled.shape[1]/2), 40, 10, 5]
  #typeNeuronHiddenLayers = ['relu', 'sigmoid','tanh']

  #se definen las posibles combinaciones de manera estatica

  numberNeuronHiddenLayer1 = getNNHL1(trainXScaled)
  numberNeuronHiddenLayer2 = getNNHL2(trainXScaled)
  numberNeuronHiddenLayer3 = getNNHL3(trainXScaled)

  typeNeuronOutLayer = getTNHL1()

  typeNeuronHiddenLayer1 = getTNHL1()
  typeNeuronHiddenLayer2 = getTNHL2()
  typeNeuronHiddenLayer3 = getTNHL3()

  for NHL in nHiddenLayer:
    for LF in lossFunction:
      for TNOL in typeNeuronOutLayer:
        if NHL == 1:
          for NNHL in numberNeuronHiddenLayer1:
            for TNHL in typeNeuronHiddenLayer1:
              configDict = generateConfigurationDictionary(nInputs, nOutputs, NHL, NNHL, TNHL, TNOL[0], LF, delay, horizon)
              aplicateMLP(configDict, trainXScaled, trainY, testXScaled, testY, nameExcelFile)
        elif NHL == 2:
          for NNHL in numberNeuronHiddenLayer2:
            for TNHL in typeNeuronHiddenLayer2:
              configDict = generateConfigurationDictionary(nInputs, nOutputs, NHL, NNHL, TNHL, TNOL[0], LF, delay, horizon)
              aplicateMLP(configDict, trainXScaled, trainY, testXScaled, testY, nameExcelFile)
        else:
          for NNHL in numberNeuronHiddenLayer3:
            for TNHL in typeNeuronHiddenLayer3:
              configDict = generateConfigurationDictionary(nInputs, nOutputs, NHL, NNHL, TNHL, TNOL[0], LF, delay, horizon)
              aplicateMLP(configDict, trainXScaled, trainY, testXScaled, testY, nameExcelFile)


def runMultipleMLP():
  predictionHorizon = 1
  stepsAR = [2]
  trainDFScaled, testDFScaled, trainDF, testDF = cargarData1()
  for stepAR in stepsAR:
    print("Ejecutando modelos con "+ str(stepAR)+" pasos")
    trainXScaled, trainY, testXScaled, testY = generateFinalData(stepAR, predictionHorizon, trainDFScaled, testDFScaled, trainDF, testDF)
    nameFile = "results/grilla_MLP_NARMAX_"+str(stepAR)+"STEPS.xlsx"
    createExcel(nameFile)
    #probar multiples modelos
    generateMultipleModelMLP(trainXScaled, trainY, testXScaled, testY, stepAR, predictionHorizon, nameFile)


runMultipleMLP()